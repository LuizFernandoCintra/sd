import zmq, time, boto.utils
from threading import Thread

##################################################################################
# PORTAS, HOST E VARIAVEIS																											 #
##################################################################################

HOST = "*"
HOST1 = "3.218.144.28"
HOST2 = "3.222.109.95"
PORT1 = "8081"
PORT2 = "8084"
PORT3 = "8085"
PORT4 = "8086"
PORT5 = "8087"
PORT6 = "8101"
PORT7 = "8102"

PREDIO = "p1"
ANDARES = ["a1", "a2", "a3", "a4", "a5"]

# Dicionario com as pessoas que podem acessar o predio com os indices sendo o andar/ou predio
p_permitidas = {}
p_permitidas[PREDIO] = []
for i in ANDARES:
	p_permitidas[i] = []


# Como funcionarios tem tratamento diferente armazena os funcionarios em uma lista diferente
func = []

# Dicionario com pessoas que entraram
p_entraram = {}
p_entraram[PREDIO] = []
for i in ANDARES:
	p_entraram[i] = []

# Dicionario com a capacidade limite
lim_cap = {}

# Dicionario com o numeros de pessoas
cont = {}

##################################################################################

##################################################################################
# THREAD QUE RECEBE DO SERVIDOR CENTRAL QUEM PODE TER ACESSO										 #
##################################################################################

class Atualiza(Thread):
# Thread que atualiza ao receber uma publicao
	def __init__ (self):
		Thread.__init__(self)

	def run(self):
		# Define uma porta para escutar as publicacoes
		# Cria o socket
		context1 = zmq.Context()
		s_atualiza = context1.socket(zmq.SUB)
		p_atualiza = "tcp://"+ HOST1 +":"+ PORT1
		s_atualiza.connect(p_atualiza)
		s_atualiza.setsockopt(zmq.SUBSCRIBE, PREDIO)
		s_atualiza.setsockopt(zmq.SUBSCRIBE, "func")

		# Espera uma publicacao
		while True:
			message = s_atualiza.recv()

			print("Atualizacao acesso: " + message)

			# Obtem os campos da mensagem
			aux = message.split(" ")

			# Se for funcionario pode acessar todos os andares
			if (aux[0] == "func"):
				# Se o funcionario foi removido
				if (aux[1] == "rm"):
					# Verifica se existe
					if (aux[2] in func):
						# Remove da lista de funcionarios presentes no andar
						func.remove(aux[2])					

				# Se o funcionario entrou no complexo
				else:
					# Verifica se ja existe
					if (aux[1] not in func):
						# Adiciona a lista de funcionario presentes no andar
						func.append(aux[1])

			# Visitante nao tem acesso a todos os lugares
			else:
				# Se o acesso foi revogado ou o visitante removido
				if (aux[1] == "rm"):
					# Verifica se existe
					if (aux[2] in p_permitidas[aux[3]]):
						p_permitidas[aux[3]].remove(aux[2])
				else:
				# Se o visitante entrou
					# Verifica se ja existe
					if (aux[1] not in p_permitidas[aux[2]]):
						p_permitidas[aux[2]].append(aux[1])
##################################################################################

##################################################################################
# THREAD QUE RECEBE DO SERVIDOR CENTRAL ATUALIZACOES DAS CAPACIDADES						 #
##################################################################################

class AtualizaCap(Thread):
# Thread que atualiza ao receber uma publicao
	def __init__ (self):
		Thread.__init__(self)

	def run(self):
		# Define uma porta para escutar as publicacoes
		# Cria o socket
		context4 = zmq.Context()
		s_atualizaU = context4.socket(zmq.SUB)
		p_atualizaU = "tcp://"+ HOST1 +":"+ PORT4
		s_atualizaU.connect(p_atualizaU)
		s_atualizaU.setsockopt(zmq.SUBSCRIBE, "'" + PREDIO + "'")
		for i in ANDARES:
			s_atualizaU.setsockopt(zmq.SUBSCRIBE, "'"+ i + "'")

		while True:
			# Recebe atualizacao
			message = s_atualizaU.recv()

			print("Atualizacao Capacidade: " + message)

			# Obtem campos
			aux = message.split(" ")
			# Atualiza o dicionario
			lim_cap[aux[0]] = int(aux[1])
##################################################################################		

##################################################################################
# THREAD QUE RECEBE ATUALIZACOES DO CONTADOR DE PESSOAS	PROVENIENTES DE REPLICAS #
##################################################################################

class AtualizaCont(Thread):
# Thread que atualiza ao receber uma publicao
	def __init__ (self, serv):
		Thread.__init__(self)
		self.serv = serv

	def run(self):
		# Se nao tem replica sai
		if not self.serv:
			return;

		# Define uma porta para escutar as publicacoes
		# Cria o socket
		context7 = zmq.Context()
		s_atualizaC = context7.socket(zmq.SUB)

		# Se conecta as replicas
		for i in self.serv:
			p_atualizaC = "tcp://"+ i +":"+ PORT7
			s_atualizaC.connect(p_atualizaC)

		s_atualizaC.setsockopt(zmq.SUBSCRIBE, PREDIO)
		for i in ANDARES:
			s_atualizaC.setsockopt(zmq.SUBSCRIBE, i)

		while True:
			# Recebe atualizacao
			message = s_atualizaC.recv()

			# Obtem campos
			aux = message.split(" ")
			
			# Cria um indice formantando da forma correta, apenas para facilitar acesso
			i = "'" + aux[0] + "'"
			
			msg_print = "Atualizacao Contador: " + message + " Contador Anterior: " + str(cont[i])

			if(aux[1] == "+"):
				# Para nao contabilizar duas vezes
				if(aux[2] not in p_entraram[aux[0]]):
					# Atualiza o dicionario
					p_entraram[aux[0]].append(aux[2])
					cont[i] += 1
			elif(aux[1] == "-"):
				# Para nao contabilizar duas vezes
				if(aux[2] in p_entraram[aux[0]]):
					# Retira da lista de pessoas presentes
					p_entraram[aux[0]].remove(aux[2])
					cont[i] -= 1

			print(msg_print + " Contador Atualizado: " + str(cont[i]))
##################################################################################

##################################################################################
#THREAD QUE ATENDE REQUISICOES DE ACESSO DO DISPOSITIVO DE ACESSO AO PREDIO E DO #
#DISPOSITIVO DE ACESSO A UM ANDAR																								 # 
##################################################################################

class Permissao(Thread):
	# Thread para atender as requisicoes do(s) dispositivo(s) de acesso
	# na entrada do predio
	def __init__ (self):
		Thread.__init__(self)

	def run(self):
		# Funciona em requisicao e resposta
		# Usa uma porta para atender as requisicoes do dispositivo de acesso
		# Cria um socket
		context2 = zmq.Context()
		p_predio = "tcp://"+ HOST +":"+ PORT2
		s_predio = context2.socket(zmq.REP)
		s_predio.bind(p_predio)
		
		# Socket para notificar as outras replicas
		context8 = zmq.Context()
		s_not = context8.socket(zmq.PUB)
		p_not = "tcp://" + HOST + ":" + PORT7
		s_not.bind(p_not)


		# Espera uma requisicao do dispositivo de acesso
		while True:
			# Recebe a requisicao do dispositivo de acesso
			message = s_predio.recv()

			# Obtem os campos
			aux = message.split(" ")

			# Cria um indice formantando da forma correta, apenas para facilitar acesso
			i = "'" + aux[1] + "'"

			msg_print = "Requisicao Acesso: " + message + " Cont. Anterior: " + str(cont[i])

			# Verifica se alguem quer entrar
			if (aux[0] == "IN"):
				#Testa Permissao
				if ((aux[2] in func or (aux[2] in p_permitidas[aux[1]] and cont[i] < lim_cap[i])) and aux[2] not in p_entraram[aux[1]]):
					# Incrementa contador
					cont[i] += 1
					# Adiciona nas pessoas que entraram
					p_entraram[aux[1]].append(aux[2])
					# Envia mensagem
					retorno = "Liberado!\n Estado do Servidor:\n  Pessoas Permitidas: " + str(p_permitidas)
					retorno += "\n  Pessoas que entraram: " + str(p_entraram)
					retorno += "\n  Contador Atual: " + str(cont)
					retorno += "\n  Limite Capacidade: " + str(lim_cap)
					
					s_predio.send(retorno)
					
					s_not.send(aux[1] + " + " + aux[2])
				else:
					retorno = "Bloqueado!\n Estado do Servidor:\n  Pessoas Permitidas: " + str(p_permitidas)
					retorno += "\n  Pessoas que entraram: " + str(p_entraram)
					retorno += "\n  Contador Atual: " + str(cont)
					retorno += "\n  Limite Capacidade: " + str(lim_cap)

					# Envia mensagem					
					s_predio.send(retorno)

			# Verifica se alguem quer sair
			elif (aux[0] == "OUT"):
				# Verfica se a pessoa esta no predio/andar
				if(aux[2] in p_entraram[aux[1]]):
					# Diminui contador
					cont[i] -= 1
					# Retira da lista de pessoas presentes
					p_entraram[aux[1]].remove(aux[2])

					retorno = "Liberado!\n Estado do Servidor:\n  Pessoas Permitidas: " + str(p_permitidas)
					retorno += "\n  Pessoas que entraram: " + str(p_entraram)
					retorno += "\n  Contador Atual: " + str(cont)
					retorno += "\n  Limite Capacidade: " + str(lim_cap)

					# Envia resposta
					s_predio.send(retorno)
					s_not.send(aux[1] + " - " + aux[2])						
				else:
					retorno = "Bloqueado!\n Estado do Servidor:\n  Pessoas Permitidas: " + str(p_permitidas)
					retorno += "\n  Pessoas que entraram: " + str(p_entraram)
					retorno += "\n  Contador Atual: " + str(cont)
					retorno += "\n  Limite Capacidade: " + str(lim_cap)
					# Envia resposta
					s_predio.send(retorno)
	
			msg_print += " Cont. Atual: " + str(cont[i]) + " Max: " + str(lim_cap[i])
			print(msg_print)					
##################################################################################

##################################################################################
# REQUISITA AO SERVIDOR CENTRAL O LIMITE DA CAPACIDADE DO PREDIO E SEUS ANDARES	 #
##################################################################################

# Para finalizar sem erros ao aperta ctlr-c
try:
	print("Obtendo Limite da Capacidade...")

	# Cria o socket
	context5 = zmq.Context()
	p_ini = "tcp://"+ HOST1 +":"+ PORT5
	s_ini = context5.socket(zmq.REQ)
	s_ini.connect(p_ini)

	# Requisita os limites para o predio
	s_ini.send("'" + PREDIO + "'")
	m = s_ini.recv()

	# Adiciona no dicionario e inicializa contador
	aux = m.split(" ")
	for a in aux:
		aux2 = a.split(":")
		lim_cap["'" + str(aux2[0]) + "'"] = int(aux2[1])
		cont["'" + str(aux2[0]) + "'"] = 0

	print("Limites obtidos.")
##################################################################################

##################################################################################
# OBTENDO OS ENDERECOS DAS REPLICAS																							 #
##################################################################################
	# Obtem o ip da maquina
	ip = boto.utils.get_instance_metadata()["public-ipv4"]

	print("Obtendo enderecos das replicas...")

	# Cria o socket
	context6 = zmq.Context()
	p_replicas = "tcp://"+ HOST2 +":"+ PORT6
	s_replicas = context6.socket(zmq.REQ)
	s_replicas.connect(p_replicas)

	# Requisita os limites para o predio
	s_replicas.send(PREDIO)
	m = s_replicas.recv()

	# Adiciona no dicionario e inicializa contador
	servs = m.split(" ")
	
	# Retira o ip da maquina
	servs.remove(ip)
	print("Replicas obtidas.")
##################################################################################

##################################################################################
# CHAMADA DAS THREADS													 																	 #
##################################################################################
	print("SERVIDOR PREDIO: " + PREDIO.upper())
	p = Permissao()
	p.daemon = True;
	atualiza = Atualiza()
	atualiza.daemon = True;
	atualizaCap = AtualizaCap()
	atualizaCap.daemon = True;
	atualizaCont = AtualizaCont(servs)
	atualizaCont.daemon = True;
	p.start()
	atualiza.start()
	atualizaCap.start()
	atualizaCont.start()

	while True:
	 time.sleep(1)
except KeyboardInterrupt:
	print("\nFinalizando...")
finally:
	s_ini.close()
	time.sleep(1)
	print("Finalizado!")
##################################################################################
