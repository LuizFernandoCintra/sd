import zmq, time
import mysql.connector
from mysql.connector import Error
from threading import Thread

##################################################################################
# PORTAS, HOST E VARIAVEIS																											 #
##################################################################################
HOST = "*"
PORT = "8100"
PORT_S = "8101"
##################################################################################

##################################################################################
# COMUNICACAO COM O BD																													 #
##################################################################################

# Conectar ao banco
try:
    connection = mysql.connector.connect(host='localhost', database='nomes', user='root', password='175932486')
    if connection.is_connected():
       db_Info = connection.get_server_info()
       print("Connected to MySQL database... MySQL Server version on ",db_Info)
       cursor = connection.cursor()
       cursor.execute("select database();")
       record = cursor.fetchone()
       print ("Your connected to - ", record)
except Error as e :
    print ("Error while connecting to MySQL", e)

# Dicionario com o numero de conexoes de cada servidor
conexoes = {}

# Obtem os servidores
try:
	print("Iniciando dicionario todos os servidores...")
	query = "SELECT endereco FROM servidor;" 
	cursor = connection.cursor()
	cursor.execute(query)
	rows1 = cursor.fetchall()

	# Cada servidor vira uma chave
	for p in rows1:
		# Inicializa com 0
		conexoes[str(p[0])] = 0

	print("Iniciado.")
except mysql.connector.Error as error :
	print(error)
	connection.rollback() #rollback if any exception occured
	print("Falha na operacao!")						
##################################################################################

##################################################################################
# THREAD ENVIA OS ENDERECOS DAS REPLICAS AO SERVIDOR														 #	
##################################################################################

class Atender_Serv(Thread):
	# Thread para atender as requisicoes do(s) dispositivo(s) de acesso
	# na entrada do complexo
	def __init__ (self):
		Thread.__init__(self)

	def run(self):
		# Funciona em requisicao e resposta
		# Usa uma porta para atender as requisicoes do dispositivo de acesso
		# Cria socket
		context2 = zmq.Context()
		p_serv = "tcp://"+ HOST +":"+ PORT_S
		s_serv = context2.socket(zmq.REP)
		s_serv.bind(p_serv)

		# Espera uma requisicao do dispositivo de acesso
		while True:
			# Recebe a requisicao do dispositivo de acesso
			m = s_serv.recv()

			# Constroi as querys
			select_query1 = "SELECT endereco FROM servidor WHERE identificador = '" + m + "';"

			try:
				#Testa Permissao funcionario vendo se ele esta no BD
				cursor = connection.cursor()
				cursor.execute(select_query1)
				rows = cursor.fetchall()

				# Verfica se o servidor requisitado existe
				if (rows):
					servs = ''
					for i in rows:
						servs += str(i[0])
						if(i != rows[-1]):
							servs += ' '

					# Envia mensagem liberando a entrada
					s_serv.send(servs)

					print("Requisicao Servidor: " + m + " Lista retornado:" + servs)

				# Se nao existe o servidor
				else:
					# Envia mensagem bloqueando a entrada
					print("Nao existe indentificador")
					s_serv.send("Identificador inexistente")

			except mysql.connector.Error as error :
				print(error)
				connection.rollback() #rollback if any exception occured
				s_serv.send("Falha na operacao!")
				continue
##################################################################################

##################################################################################
# THREAD QUE ATENDE REQUISICOES DE ENTRADA DO DISPOSITIVO DE ACESSO DA ENTRADA DO#	
#	COMPLEXO																		 																	 #
##################################################################################

class Atender_Req(Thread):
	# Thread para atender as requisicoes do(s) dispositivo(s) de acesso
	# na entrada do complexo
	def __init__ (self):
		Thread.__init__(self)

	def run(self):
		global conexoes

		# Funciona em requisicao e resposta
		# Usa uma porta para atender as requisicoes do dispositivo de acesso
		# Cria socket
		context1 = zmq.Context()
		p_rep = "tcp://"+ HOST +":"+ PORT
		s_rep = context1.socket(zmq.REP)
		s_rep.bind(p_rep)

		# Espera uma requisicao do dispositivo de acesso
		while True:
			# Recebe a requisicao do dispositivo de acesso
			m = s_rep.recv()

			# Constroi as querys
			select_query1 = "SELECT endereco FROM servidor WHERE identificador = '" + m + "';"

			try:
				#Testa Permissao funcionario vendo se ele esta no BD
				cursor = connection.cursor()
				cursor.execute(select_query1)
				rows = cursor.fetchall()

				# Verfica se o servidor requisitado existe
				if (rows):
					menor = ''
					menor_conexoes = 1000000

					for i in rows:
						if(conexoes[str(i[0])] < menor_conexoes):
							menor = str(i[0])
							menor_conexoes = conexoes[menor]

					# Envia mensagem liberando a entrada
					s_rep.send(menor)

					conexoes[menor] = conexoes[menor] + 1
				# Se nao existe o servidor
				else:
					menor = "Identificador inexistente"
					# Envia mensagem bloqueando a entrada
					s_rep.send(menor)

			except mysql.connector.Error as error :
				print(error)
				connection.rollback() #rollback if any exception occured
				s_rep.send("Falha na operacao!")
				continue						

			print("Requisicao: " + m + " Endereco retornado:" + menor)
##################################################################################

##################################################################################
# CHAMADA DAS THREADS													 																	 #
##################################################################################
try:
	req = Atender_Req()
	req.daemon = True
	req_s = Atender_Serv()
	req_s.daemon = True
	req.start()
	req_s.start()

	while True:
	 time.sleep(1)
except KeyboardInterrupt:
	print("\nFinalizando...")
finally:
	time.sleep(1)
	print("Finalizado!")
##################################################################################
