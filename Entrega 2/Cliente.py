import zmq, time

##################################################################################
# PORTAS, HOST E VARIAVEIS																											 #
##################################################################################

HOST = "3.222.109.95"
PORT = "8100"
PORT_C = "8084"

##################################################################################

##################################################################################
# CRIACAO DE SOCKETS																														 #
##################################################################################

context1 = zmq.Context()
p1 = "tcp://"+ HOST +":"+ PORT
s1 = context1.socket(zmq.REQ)
s1.connect(p1)
##################################################################################

##################################################################################
# EXECUCAO DO ADMIN																															 #
##################################################################################
try:
	while True:
		print("\n\n##############################################################")
		print("#                             MENU                           #")
		print("##############################################################")

		# Opcoes de operacoes do admin
		op = input("\n\nEscolha a operacao:\n 1 - Entrar\n 2 - Sair \n 3 - Fechar\n ");

		# Entrar
		if(op == 1):
			cpf = raw_input("\n Digite o cpf do cliente ou VOLTAR para retornar:\n ");
			if(cpf != "VOLTAR"):
				local = raw_input("\n Digite o lugar onde o cliente quer entrar: (id do complexo/predio/andar)\n ");
				
				# Obtem o endereco do servidor central
				s1.send(local)
				HOST_C = s1.recv()

				# Se conecta ao servidor central
				context = zmq.Context()
				p = "tcp://"+ HOST_C +":"+ PORT_C
				s = context.socket(zmq.REQ)
				s.connect(p)
				
				if(local == "c1"):
					msg = "IN " + cpf
				else:
					msg = "IN " + local + " " + cpf

				s.send(msg)
				res = s.recv()
				print("\n-------------------------------------------------------------------------------------------")
				print("\n HOST: " + HOST_C + "\n " + res)
				print("\n-------------------------------------------------------------------------------------------\n")
				s.close()
		# Sair
		elif(op == 2):
			cpf = raw_input("\n Digite o cpf do cliente ou VOLTAR para retornar:\n ");
			if(cpf != "VOLTAR"):
				local = raw_input("\n Digite o lugar onde o cliente esta:(id do complexo/predio/andar)\n ");
				
				# Obtem o endereco do servidor central
				s1.send(local)
				HOST_C = s1.recv()

				# Se conecta ao servidor central
				context = zmq.Context()
				p = "tcp://"+ HOST_C +":"+ PORT_C
				s = context.socket(zmq.REQ)
				s.connect(p)
				
				if(local == "c1"):
					msg = "OUT " + cpf
				else:
					msg = "OUT " + local + " " + cpf

				s.send(msg)
				res = s.recv()
				print("\n-------------------------------------------------------------------------------------------")
				print("\n " + res)				
				print("\n-------------------------------------------------------------------------------------------\n")
				s.close()
		# Fechar
		elif(op == 3):
			break;

		print("\n\n")

except KeyboardInterrupt:
	print("\nFinalizando...")
finally:
	print("Finalizado!")
##################################################################################
