import zmq, time
import mysql.connector
from mysql.connector import Error
from random import randint
from threading import Thread

##################################################################################
# PORTAS, HOST E VARIAVEIS																											 #
##################################################################################

HOST = "3.222.109.95"
PORT = "8100"
PORT_C = "8084"
##################################################################################

##################################################################################
#THREAD QUE EXECUTA A SIMULACAO DE ACESSO A ALGUM LUGAR DA EMPRESA							 #
##################################################################################

class Simul(Thread):
	# Thread para atender as requisicoes do(s) dispositivo(s) de acesso
	# na entrada do predio
	def __init__ (self, serv, n, pessoas):
		Thread.__init__(self)
		self.serv = serv
		self.n = n
		self.pessoas = pessoas
		self.s = 0

	def run(self):
		context1 = zmq.Context()
		p1 = "tcp://"+ HOST +":"+ PORT
		s1 = context1.socket(zmq.REQ)
		s1.connect(p1)

		# Obtem o endereco do servidor central
		s1.send(self.serv)
		HOST_C = s1.recv()

		# Se conecta ao servidor central
		context = zmq.Context()
		p = "tcp://"+ HOST_C +":"+ PORT_C
		s = context.socket(zmq.REQ)
		s.connect(p)
		
		for i in range(1, self.n):
			# Chance de 50% de uma operacao ser in
			if(randint(0,100) < 50):
				op = "IN "
			else:
				op = "OUT "

			
			if(self.serv == "c1"):
				msg = op
			else:
				msg = op + self.serv + " "			

			# Escolhe uma pessoa aleatoria
			i = randint(0, len(self.pessoas)-1)

			msg += self.pessoas[i]						

			temp_ini = time.time()
			s.send(msg)
			a = s.recv()
			temp_fim = time.time()

			self.s += temp_fim - temp_ini
		print(self.serv + ": " + str(self.s))
##################################################################################

##################################################################################
# CRIACAO DE UMA LISTA COM NOME DOS FUNCIONARIO E VISITANTES										 #
##################################################################################

# Conectar ao banco
try:
    connection = mysql.connector.connect(host='localhost', database='empresa', user='root', password='175932486')
    if connection.is_connected():
       db_Info = connection.get_server_info()
       print("Connected to MySQL database... MySQL Server version on ",db_Info)
       cursor = connection.cursor()
       cursor.execute("select database();")
       record = cursor.fetchone()
       print ("Your connected to - ", record)
except Error as e :
    print ("Error while connecting to MySQL", e)

# Lista com todos os funcionario e visitantes
pessoas = []

# Obtem os predios
try:
	print("Iniciando a lista com o nome dos funcionario e visitantes...")
	predio_query = "SELECT func_id FROM func;" 
	cursor = connection.cursor()
	cursor.execute(predio_query)
	rows1 = cursor.fetchall()

	# Formatando
	for p in rows1:
		pessoas.append(str(p[0]))

	predio_query = "SELECT vis_id FROM visitante;" 
	cursor = connection.cursor()
	cursor.execute(predio_query)
	rows1 = cursor.fetchall()

	# Formatando
	for p in rows1:
		pessoas.append(str(p[0]))

	print("Iniciado.")
except mysql.connector.Error as error :
	print(error)
	connection.rollback() #rollback if any exception occured
	print("Falha na operacao!")						
##################################################################################

##################################################################################
# EXECUCAO DAS THREADS																													 #
###################################s###############################################
try:
	print("SIMULACAO")
	t1 = Simul('c1', 100000, pessoas)
	t1.daemon = True
	t2 = Simul('p1', 100000, pessoas)
	t2.daemon = True
	t3 = Simul('a1', 100000, pessoas)
	t3.daemon = True
	t4 = Simul('a2', 100000, pessoas)
	t4.daemon = True
	t5 = Simul('a3', 100000, pessoas)
	t5.daemon = True
	t6 = Simul('a4', 100000, pessoas)
	t6.daemon = True
	t7 = Simul('a5', 100000, pessoas)
	t7.daemon = True
	t1.start()
	t2.start()
	t3.start()
	t4.start()
	t5.start()
	t6.start()
	t7.start()

	while True:
		time.sleep(1)
except KeyboardInterrupt:
	print("\nFinalizando...")
finally:
	time.sleep(1)
	print("Finalizado!")
