drop database if exists nomes;
create database nomes;
use nomes;

create table servidor(
endereco varchar(15) not null,
identificador varchar(30) not null,
primary key(identificador, endereco)
);

insert into servidor values ('3.218.144.28', 'c1');
insert into servidor values ('3.208.188.165', 'p1');
insert into servidor values ('52.7.54.103', 'p1');
insert into servidor values ('3.208.188.165', 'a1');
insert into servidor values ('3.208.188.165', 'a2');
insert into servidor values ('3.208.188.165', 'a3');
insert into servidor values ('3.208.188.165', 'a4');
insert into servidor values ('3.208.188.165', 'a5');
insert into servidor values ('52.7.54.103', 'a1');
insert into servidor values ('52.7.54.103', 'a2');
insert into servidor values ('52.7.54.103', 'a3');
insert into servidor values ('52.7.54.103', 'a4');
insert into servidor values ('52.7.54.103', 'a5');
