drop database if exists empresa;
create database empresa;
use empresa;

create table func(
func_id varchar(11) not null,
nome varchar(30) not null,
primary key(func_id)
);

create table admin(
admin_id varchar(11) not null,
primary key(admin_id),
foreign key (admin_id) references func(func_id)
);

create table visitante(
vis_id varchar(11) not null,
nome varchar(30) not null,
primary key(vis_id)
);

create table complexo(
id_complexo varchar(10) not null,
capacidade int not null,
primary key(id_complexo)
);

create table predio(
id_predio varchar(10) not null,
capacidade int not null,
primary key(id_predio)
);

create table tem_acesso(
vis_id varchar(11) not null references visitante(vis_id),
id_andar varchar(10) references andar(id_andar),
id_predio varchar(10) not null references predio(id_predio)
);

create table andar(
id_andar varchar(10) not null,
capacidade int not null,
primary key(id_andar)
);

create table esta_no(
id_andar varchar(10) not null references andar(id_andar),
id_predio varchar(10) not null references predio(id_predio)
);

insert into func values ('05505522215', 'Admin');
insert into admin values('05505522215');

insert into func values ('12345678910', 'Pessoa01');
insert into func values ('12345678911', 'Pessoa02');
insert into func values ('12345678912', 'Pessoa03');
insert into func values ('12345678913', 'Pessoa04');
insert into func values ('12345678914', 'Pessoa05');
insert into func values ('12345678915', 'Pessoa06');
insert into func values ('12345678916', 'Pessoa07');
insert into func values ('12345678917', 'Pessoa08');
insert into func values ('01836383930', 'Pessoa09');
insert into func values ('75930048273', 'Pessoa10');
insert into func values ('94927162480', 'Pessoa11');
insert into func values ('23648604827', 'Pessoa12');
insert into func values ('84927118494', 'Pessoa13');
insert into func values ('04846283909', 'Pessoa14');
insert into func values ('37998752101', 'Pessoa15');
insert into func values ('37998752102', 'Pessoa16');
insert into func values ('37998752103', 'Pessoa17');
insert into func values ('37998752104', 'Pessoa18');
insert into func values ('37998752105', 'Pessoa19');
insert into func values ('37998752106', 'Pessoa20');
insert into func values ('37998752107', 'Pessoa21');
insert into func values ('37998752108', 'Pessoa22');
insert into func values ('37998752109', 'Pessoa23');
insert into func values ('37998752110', 'Pessoa24');
insert into func values ('37998752111', 'Pessoa25');
insert into func values ('37998752112', 'Pessoa26');
insert into func values ('43567852501', 'Pessoa27');
insert into func values ('43567852502', 'Pessoa28');
insert into func values ('43567852503', 'Pessoa29');
insert into func values ('43567852504', 'Pessoa30');
insert into func values ('43567852505', 'Pessoa31');
insert into func values ('43567852506', 'Pessoa32');
insert into func values ('43567852507', 'Pessoa33');
insert into func values ('43567852508', 'Pessoa34');
insert into func values ('43567852509', 'Pessoa35');
insert into func values ('43567852510', 'Pessoa36');
insert into func values ('43567852511', 'Pessoa37');
insert into func values ('43567852512', 'Pessoa38');
insert into func values ('56789021211', 'Pessoa39');
insert into func values ('56789021212', 'Pessoa40');
insert into func values ('56789021213', 'Pessoa41');
insert into func values ('56789021214', 'Pessoa42');
insert into func values ('56789021215', 'Pessoa43');
insert into func values ('56789021216', 'Pessoa44');
insert into func values ('56789021217', 'Pessoa45');
insert into func values ('56789021218', 'Pessoa46');
insert into func values ('18171609601', 'Pessoa47');
insert into func values ('18171609602', 'Pessoa48');
insert into func values ('18171609603', 'Pessoa49');
insert into func values ('18171609604', 'Pessoa50');
insert into func values ('18171609605', 'Pessoa51');
insert into func values ('18171609606', 'Pessoa52');
insert into func values ('18171609607', 'Pessoa53');
insert into func values ('18171609608', 'Pessoa54');
insert into func values ('18171609609', 'Pessoa55');
insert into func values ('18171609610', 'Pessoa56');
insert into func values ('18171609611', 'Pessoa57');
insert into func values ('18171609612', 'Pessoa58');
insert into func values ('18171609613', 'Pessoa59');
insert into func values ('18171609614', 'Pessoa60');
insert into func values ('48551739701', 'Pessoa61');
insert into func values ('48551739702', 'Pessoa61');
insert into func values ('48551739703', 'Pessoa62');
insert into func values ('48551739704', 'Pessoa63');
insert into func values ('48551739705', 'Pessoa64');
insert into func values ('48551739706', 'Pessoa65');
insert into func values ('48551739707', 'Pessoa66');
insert into func values ('56789021228', 'Pessoa67');
insert into func values ('48551739709', 'Pessoa68');
insert into func values ('48551739710', 'Pessoa69');
insert into func values ('78901284610', 'Pessoa70');
insert into func values ('78901284611', 'Pessoa71');
insert into func values ('78901284612', 'Pessoa72');
insert into func values ('78901284613', 'Pessoa73');
insert into func values ('78901284614', 'Pessoa74');
insert into func values ('78901284615', 'Pessoa75');
insert into func values ('78901284616', 'Pessoa76');
insert into func values ('78901284617', 'Pessoa77');
insert into func values ('78901284618', 'Pessoa78');
insert into func values ('78901284619', 'Pessoa79');
insert into func values ('82638451750', 'Pessoa80');
insert into func values ('82638451751', 'Pessoa81');
insert into func values ('82638451752', 'Pessoa82');
insert into func values ('82638451753', 'Pessoa83');
insert into func values ('82638451754', 'Pessoa84');
insert into func values ('82638451755', 'Pessoa85');
insert into func values ('82638451756', 'Pessoa86');
insert into func values ('82638451757', 'Pessoa87');
insert into func values ('82638451758', 'Pessoa88');
insert into func values ('82638451759', 'Pessoa89');
insert into func values ('42638718980', 'Pessoa90');
insert into func values ('42638718981', 'Pessoa91');
insert into func values ('42638718982', 'Pessoa92');
insert into func values ('42638718983', 'Pessoa93');
insert into func values ('42638718984', 'Pessoa94');
insert into func values ('42638718985', 'Pessoa95');
insert into func values ('42638718986', 'Pessoa96');
insert into func values ('42638718987', 'Pessoa97');
insert into func values ('42638718988', 'Pessoa98');
insert into func values ('42638718989', 'Pessoa99');
insert into func values ('03846284070', 'Pessoa100');
insert into func values ('03846284071', 'Pessoa101');
insert into func values ('03846284072', 'Pessoa102');
insert into func values ('03846284073', 'Pessoa103');
insert into func values ('03846284074', 'Pessoa104');
insert into func values ('03846284075', 'Pessoa105');
insert into func values ('03846284076', 'Pessoa106');
insert into func values ('03846284077', 'Pessoa107');
insert into func values ('03846284078', 'Pessoa108');
insert into func values ('03846284079', 'Pessoa109');
insert into func values ('67556710000', 'Pessoa110');
insert into func values ('67556710001', 'Pessoa111');
insert into func values ('67556710002', 'Pessoa112');
insert into func values ('67556710003', 'Pessoa113');
insert into func values ('67556710004', 'Pessoa114');
insert into func values ('67556710005', 'Pessoa115');
insert into func values ('67556710006', 'Pessoa116');
insert into func values ('67556710007', 'Pessoa117');
insert into func values ('67556710008', 'Pessoa118');
insert into func values ('67556710009', 'Pessoa119');
insert into func values ('87729301830', 'Pessoa120');
insert into func values ('87729301831', 'Pessoa121');
insert into func values ('87729301832', 'Pessoa122');
insert into func values ('87729301833', 'Pessoa123');
insert into func values ('87729301834', 'Pessoa124');
insert into func values ('87729301835', 'Pessoa125');
insert into func values ('87729301836', 'Pessoa126');
insert into func values ('87729301837', 'Pessoa127');
insert into func values ('87729301838', 'Pessoa128');
insert into func values ('87729301839', 'Pessoa129');
insert into func values ('34568715240', 'Pessoa130');
insert into func values ('34568715241', 'Pessoa131');
insert into func values ('34568715242', 'Pessoa132');
insert into func values ('34568715243', 'Pessoa133');
insert into func values ('34568715244', 'Pessoa134');
insert into func values ('34568715245', 'Pessoa135');
insert into func values ('34568715246', 'Pessoa136');
insert into func values ('34568715247', 'Pessoa137');
insert into func values ('34568715248', 'Pessoa138');
insert into func values ('34568715249', 'Pessoa139');
insert into func values ('92327452970', 'Pessoa140');
insert into func values ('92327452971', 'Pessoa141');
insert into func values ('92327452972', 'Pessoa142');
insert into func values ('92327452973', 'Pessoa143');
insert into func values ('92327452974', 'Pessoa144');
insert into func values ('92327452975', 'Pessoa145');
insert into func values ('92327452976', 'Pessoa146');
insert into func values ('92327452977', 'Pessoa147');
insert into func values ('92327452978', 'Pessoa148');
insert into func values ('92327452979', 'Pessoa149');
insert into func values ('37891735130', 'Pessoa150');
insert into func values ('37891735131', 'Pessoa151');
insert into func values ('37891735132', 'Pessoa152');
insert into func values ('37891735133', 'Pessoa153');
insert into func values ('37891735134', 'Pessoa154');
insert into func values ('37891735135', 'Pessoa155');
insert into func values ('37891735136', 'Pessoa156');
insert into func values ('37891735137', 'Pessoa157');
insert into func values ('37891735138', 'Pessoa158');
insert into func values ('37891735139', 'Pessoa159');
insert into func values ('84629147250', 'Pessoa160');

insert into visitante values ('35353535911', 'Pessoas01');
insert into visitante values ('35353535912', 'Pessoas02');
insert into visitante values ('35353535913', 'Pessoas03');
insert into visitante values ('35353535914', 'Pessoas04');
insert into visitante values ('35353535915', 'Pessoas05');
insert into visitante values ('35353535916', 'Pessoas06');
insert into visitante values ('35353535917', 'Pessoas07');
insert into visitante values ('35353535918', 'Pessoas08');
insert into visitante values ('35353535919', 'Pessoas09');
insert into visitante values ('63456354210', 'Pessoas10');
insert into visitante values ('63456354211', 'Pessoas11');
insert into visitante values ('63456354212', 'Pessoas12');
insert into visitante values ('63456354213', 'Pessoas13');
insert into visitante values ('63456354214', 'Pessoas14');
insert into visitante values ('63456354215', 'Pessoas15');
insert into visitante values ('63456354216', 'Pessoas16');
insert into visitante values ('63456354217', 'Pessoas17');
insert into visitante values ('63456354218', 'Pessoas18');
insert into visitante values ('63456354219', 'Pessoas19');
insert into visitante values ('98178456330', 'Pessoas20');
insert into visitante values ('98178456331', 'Pessoas21');
insert into visitante values ('98178456332', 'Pessoas22');
insert into visitante values ('98178456333', 'Pessoas23');
insert into visitante values ('98178456334', 'Pessoas24');
insert into visitante values ('98178456335', 'Pessoas25');
insert into visitante values ('98178456336', 'Pessoas26');
insert into visitante values ('98178456337', 'Pessoas27');
insert into visitante values ('98178456338', 'Pessoas28');
insert into visitante values ('98178456339', 'Pessoas29');
insert into visitante values ('48762548010', 'Pessoas30');
insert into visitante values ('48762548011', 'Pessoas31');
insert into visitante values ('48762548012', 'Pessoas32');
insert into visitante values ('48762548013', 'Pessoas33');
insert into visitante values ('48762548014', 'Pessoas34');
insert into visitante values ('48762548015', 'Pessoas35');
insert into visitante values ('48762548016', 'Pessoas36');
insert into visitante values ('48762548017', 'Pessoas37');
insert into visitante values ('48762548018', 'Pessoas38');
insert into visitante values ('48762548019', 'Pessoas39');
insert into visitante values ('83835784590', 'Pessoas40');
insert into visitante values ('83835784591', 'Pessoas41');
insert into visitante values ('83835784592', 'Pessoas42');
insert into visitante values ('83835784593', 'Pessoas43');
insert into visitante values ('83835784594', 'Pessoas44');
insert into visitante values ('83835784595', 'Pessoas45');
insert into visitante values ('83835784596', 'Pessoas46');
insert into visitante values ('83835784597', 'Pessoas47');
insert into visitante values ('83835784598', 'Pessoas48');
insert into visitante values ('83835784599', 'Pessoas49');
insert into visitante values ('84728939770', 'Pessoas50');
insert into visitante values ('84728939771', 'Pessoas51');
insert into visitante values ('84728939772', 'Pessoas52');
insert into visitante values ('84728939773', 'Pessoas53');
insert into visitante values ('84728939774', 'Pessoas54');
insert into visitante values ('84728939775', 'Pessoas55');
insert into visitante values ('84728939776', 'Pessoas56');
insert into visitante values ('84728939777', 'Pessoas57');
insert into visitante values ('84728939778', 'Pessoas58');
insert into visitante values ('84728939779', 'Pessoas59');
insert into visitante values ('92737659503', 'Pessoas60');


insert into complexo values ('c1', 1500);

insert into predio values ('p1', 500);
insert into predio values ('p2', 500);
insert into predio values ('p3', 500);

insert into andar values ('a1', 100);
insert into andar values ('a2', 100);
insert into andar values ('a3', 100);
insert into andar values ('a4', 100);
insert into andar values ('a5', 100);

insert into andar values ('a6', 100);
insert into andar values ('a7', 100);
insert into andar values ('a8', 100);
insert into andar values ('a9', 100);
insert into andar values ('a10', 100);

insert into andar values ('a11', 100);
insert into andar values ('a12', 100);
insert into andar values ('a13', 100);
insert into andar values ('a14', 100);
insert into andar values ('a15', 100);

insert into esta_no values ('a1', 'p1');
insert into esta_no values ('a2', 'p1');
insert into esta_no values ('a3', 'p1');
insert into esta_no values ('a4', 'p1');
insert into esta_no values ('a5', 'p1');

insert into esta_no values ('a6', 'p2');
insert into esta_no values ('a7', 'p2');
insert into esta_no values ('a8', 'p2');
insert into esta_no values ('a9', 'p2');
insert into esta_no values ('a10', 'p2');

insert into esta_no values ('a11', 'p3');
insert into esta_no values ('a12', 'p3');
insert into esta_no values ('a13', 'p3');
insert into esta_no values ('a14', 'p3');
insert into esta_no values ('a15', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('35353535911', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('35353535911', 'p3');

insert into tem_acesso values ('35353535911', 'a1', 'p1');
insert into tem_acesso values ('35353535911', 'a12', 'p3');
insert into tem_acesso values ('35353535911', 'a4', 'p1');

insert into tem_acesso(vis_id, id_predio) values ('35353535912', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('35353535912', 'p2');
insert into tem_acesso(vis_id, id_predio) values ('35353535912', 'p3');

insert into tem_acesso values ('35353535912', 'a6', 'p2');
insert into tem_acesso values ('35353535912', 'a7', 'p2');
insert into tem_acesso values ('35353535912', 'a8', 'p2');
insert into tem_acesso values ('35353535912', 'a1', 'p1');
insert into tem_acesso values ('35353535912', 'a13', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('35353535913', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('35353535913', 'p2');

insert into tem_acesso values ('35353535913', 'a1', 'p1');
insert into tem_acesso values ('35353535913', 'a6', 'p2');
insert into tem_acesso values ('35353535913', 'a7', 'p2');

insert into tem_acesso(vis_id, id_predio) values ('35353535914', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('35353535914', 'p3');

insert into tem_acesso values ('35353535914', 'a1', 'p1');
insert into tem_acesso values ('35353535914', 'a11', 'p3');
insert into tem_acesso values ('35353535914', 'a12', 'p3');
insert into tem_acesso values ('35353535914', 'a13', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('35353535915', 'p3');
insert into tem_acesso(vis_id, id_predio) values ('35353535915', 'p1');

insert into tem_acesso values ('35353535915', 'a1', 'p1');
insert into tem_acesso values ('35353535915', 'a13', 'p3');
insert into tem_acesso values ('35353535915', 'a15', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('35353535916', 'p3');

insert into tem_acesso values ('35353535916', 'a11', 'p3');
insert into tem_acesso values ('35353535916', 'a13', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('35353535917', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('35353535917', 'p2');
insert into tem_acesso(vis_id, id_predio) values ('35353535917', 'p3');

insert into tem_acesso values ('35353535917', 'a1', 'p1');
insert into tem_acesso values ('35353535917', 'a5', 'p1');
insert into tem_acesso values ('35353535917', 'a8', 'p2');
insert into tem_acesso values ('35353535917', 'a14', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('35353535918', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('35353535918', 'p3');

insert into tem_acesso values ('35353535918', 'a1', 'p1');
insert into tem_acesso values ('35353535918', 'a15', 'p3');
insert into tem_acesso values ('35353535918', 'a12', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('35353535919', 'p1');

insert into tem_acesso values ('35353535919', 'a1', 'p1');
insert into tem_acesso values ('35353535919', 'a5', 'p1');

insert into tem_acesso(vis_id, id_predio) values ('63456354210', 'p3');

insert into tem_acesso values ('63456354210', 'a11', 'p3');
insert into tem_acesso values ('63456354210', 'a14', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('63456354211', 'p2');

insert into tem_acesso values ('63456354211', 'a9', 'p2');

insert into tem_acesso(vis_id, id_predio) values ('63456354212', 'p1');

insert into tem_acesso values ('63456354212', 'a1', 'p1');

insert into tem_acesso(vis_id, id_predio) values ('63456354213', 'p1');

insert into tem_acesso values ('63456354213', 'a1', 'p1');
insert into tem_acesso values ('63456354213', 'a2', 'p1');

insert into tem_acesso(vis_id, id_predio) values ('63456354214', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('63456354214', 'p2');

insert into tem_acesso values ('63456354214', 'a1', 'p1');
insert into tem_acesso values ('63456354214', 'a8', 'p2');
insert into tem_acesso values ('63456354214', 'a9', 'p2');

insert into tem_acesso(vis_id, id_predio) values ('63456354215', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('63456354215', 'p2');

insert into tem_acesso values ('63456354215', 'a1', 'p1');
insert into tem_acesso values ('63456354215', 'a7', 'p2');
insert into tem_acesso values ('63456354215', 'a10', 'p2');

insert into tem_acesso(vis_id, id_predio) values ('63456354216', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('63456354216', 'p2');
insert into tem_acesso(vis_id, id_predio) values ('63456354216', 'p3');

insert into tem_acesso values ('63456354216', 'a1', 'p1');
insert into tem_acesso values ('63456354216', 'a5', 'p1');
insert into tem_acesso values ('63456354216', 'a4', 'p1');
insert into tem_acesso values ('63456354216', 'a10', 'p2');
insert into tem_acesso values ('63456354216', 'a14', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('63456354217', 'p2');
insert into tem_acesso(vis_id, id_predio) values ('63456354217', 'p3');

insert into tem_acesso values ('63456354217', 'a8', 'p2');
insert into tem_acesso values ('63456354217', 'a9', 'p2');
insert into tem_acesso values ('63456354217', 'a15', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('63456354218', 'p1');

insert into tem_acesso values ('63456354218', 'a1', 'p1');

insert into tem_acesso(vis_id, id_predio) values ('63456354219', 'p1');

insert into tem_acesso values ('63456354219', 'a1', 'p1');

insert into tem_acesso(vis_id, id_predio) values ('98178456330', 'p3');

insert into tem_acesso values ('98178456330', 'a14', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('98178456331', 'p1');

insert into tem_acesso values ('98178456331', 'a1', 'p1');
insert into tem_acesso values ('98178456331', 'a4', 'p1');

insert into tem_acesso(vis_id, id_predio) values ('98178456332', 'p1');

insert into tem_acesso values ('98178456332', 'a1', 'p1');

insert into tem_acesso(vis_id, id_predio) values ('98178456333', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('98178456333', 'p2');
insert into tem_acesso(vis_id, id_predio) values ('98178456333', 'p3');

insert into tem_acesso values ('98178456333', 'a1', 'p1');
insert into tem_acesso values ('98178456333', 'a10', 'p2');
insert into tem_acesso values ('98178456333', 'a11', 'p3');
insert into tem_acesso values ('98178456333', 'a15', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('98178456334', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('98178456334', 'p2');

insert into tem_acesso values ('98178456334', 'a1', 'p1');
insert into tem_acesso values ('98178456334', 'a7', 'p2');
insert into tem_acesso values ('98178456334', 'a9', 'p2');

insert into tem_acesso(vis_id, id_predio) values ('98178456335', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('98178456335', 'p3');

insert into tem_acesso values ('98178456335', 'a1', 'p1');
insert into tem_acesso values ('98178456335', 'a15', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('98178456336', 'p3');

insert into tem_acesso values ('98178456336', 'a14', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('98178456337', 'p1');

insert into tem_acesso values ('98178456337', 'a1', 'p1');

insert into tem_acesso(vis_id, id_predio) values ('98178456338', 'p1');

insert into tem_acesso values ('98178456338', 'a1', 'p1');
insert into tem_acesso values ('98178456338', 'a3', 'p1');

insert into tem_acesso(vis_id, id_predio) values ('98178456339', 'p1');
insert into tem_acesso(vis_id, id_predio) values ('98178456339', 'p3');

insert into tem_acesso values ('98178456339', 'a13', 'p3');
insert into tem_acesso values ('98178456339', 'a1', 'p1');
