drop database if exists empresa;
create database empresa;
use empresa;

create table func(
func_id varchar(11) not null,
nome varchar(30) not null,
primary key(func_id)
);

create table admin(
admin_id varchar(11) not null,
primary key(admin_id),
foreign key (admin_id) references func(func_id)
);

create table visitante(
vis_id varchar(11) not null,
nome varchar(30) not null,
primary key(vis_id)
);

create table complexo(
id_complexo varchar(10) not null,
capacidade int not null,
primary key(id_complexo)
);

create table predio(
id_predio varchar(10) not null,
capacidade int not null,
primary key(id_predio)
);

create table tem_acesso(
vis_id varchar(11) not null references visitante(vis_id),
id_andar varchar(10) references andar(id_andar),
id_predio varchar(10) not null references predio(id_predio)
);

create table andar(
id_andar varchar(10) not null,
capacidade int not null,
primary key(id_andar)
);

create table esta_no(
id_andar varchar(10) not null references andar(id_andar),
id_predio varchar(10) not null references predio(id_predio)
);

insert into func values ('05505522215', 'Admin');
insert into admin values('05505522215');

insert into func values ('12345678910', 'Pessoa11');
insert into func values ('12345678911', 'Pessoa12');
insert into func values ('12345678912', 'Pessoa3');
insert into func values ('12345678913', 'Pessoa4');
insert into func values ('12345678914', 'Pessoa5');
insert into func values ('12345678915', 'Pessoa6');
insert into func values ('12345678916', 'Pessoa7');
insert into func values ('12345678917', 'Pessoa8');

insert into visitante values ('12345678918', 'Pessoa9');
insert into visitante values ('12345678919', 'Pessoa10');

insert into complexo values ('c1', 8);

insert into predio values ('p3', 4);

insert into andar values ('a11', 2);
insert into andar values ('a12', 2);
insert into andar values ('a13', 1);
insert into andar values ('a14', 1);
insert into andar values ('a15', 1);

insert into esta_no values ('a11', 'p3');
insert into esta_no values ('a12', 'p3');
insert into esta_no values ('a13', 'p3');
insert into esta_no values ('a14', 'p3');
insert into esta_no values ('a15', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('12345678918', 'p3');

insert into tem_acesso values ('12345678918', 'a11', 'p3');

insert into tem_acesso(vis_id, id_predio) values ('12345678919', 'p3');
insert into tem_acesso values ('12345678919', 'a11', 'p3');
