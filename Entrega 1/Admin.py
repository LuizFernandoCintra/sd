import zmq, time

##################################################################################
# PORTAS, HOST E VARIAVEIS																											 #
##################################################################################

HOST = "3.212.16.17"
PORT = "8082"
##################################################################################

##################################################################################
# CRIACAO DE SOCKETS																														 #
##################################################################################

context = zmq.Context()
p = "tcp://"+ HOST +":"+ PORT
s = context.socket(zmq.REQ)

s.connect(p)

##################################################################################

##################################################################################
# FUNCAO INSERIR																																 #
##################################################################################

def inserir():
	while True:
		print("\n\n##############################################################")
		print("#                           INSERIR                          #")
		print("##############################################################")

		# Opcoes de operacoes do admin
		op = input("\n\nEscolha o que inserir :\n 1 - Predio\n 2 - Andar\n 3 - Visitante \n 4 - Funcionario\n 5 - Voltar\n ");

		# Insere predio
		if(op == 1):
			# Adiciona a operacao, tabela e atributos
			msg = 'insert/predio/id_predio,capacidade/'
			# Obtem o id do predio
			p_id = raw_input("\n\n Digite o id do predio\n ");
			# Insere o id, ja formatado para consulta sql
			msg += "'" + p_id + "',"
			# Obtem a capacidade			
			cap = raw_input(" Digite a capacidade\n ");
			# Insere a capacidade, ja formatado para consulta sql
			msg += cap			

			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()
			print("\n\n")
			print message
			print("\n\n")

			# Para cada andar do predio envia uma requisicao
			while True:
				# Adiciona a operacao, tabela e atributos
				msg = 'insert/andar/id_andar,capacidade/'
				# Adiciona a operacao, tabela e atributos
				msg1 = 'insert/esta_no/id_andar,id_predio/'				

				# Obtem o id do andar
				a_id = raw_input("\n\n Digite o id do andar\n ");
				# Insere o id, ja formatado para consulta sql
				msg += "'" + a_id + "',"
				msg1 += "'" + a_id + "'," + "'" + p_id + "'"

				# Obtem a capacidade			
				a_cap = raw_input(" Digite a capacidade\n ");
				# Insere a capacidade, ja formatado para consulta sql
				msg += a_cap			
			
				# envia mensagem
				s.send(msg)
				# recebe se obteve sucesso
				message = s.recv()

				print("\n\n")
				print message
				print("\n\n")

				# envia mensagem
				s.send(msg1)
				# recebe se obteve sucesso
				message = s.recv()

				print("\n\n")
				print message
				print("\n\n")
			
				# Verfica se quer inserir outro andar no predio
				cont = raw_input(" Inserir outro andar? S/N\n ");
				if (cont == 'N'):
					break

		# Inserir andar
		elif(op == 2):

			# Adiciona a operacao, tabela e atributos
			msg = 'insert/andar/id_andar,capacidade/'
			# Adiciona a operacao, tabela e atributos
			msg1 = 'insert/esta_no/id_andar,id_predio/'				

			# Obtem o id do andar
			a_id = raw_input("\n\n Digite o id do andar\n ");
			# Insere o id, ja formatado para consulta sql
			msg += "'" + a_id + "',"
			msg1 += "'" + a_id + "',"
			# Obtem a capacidade			
			a_cap = raw_input(" Digite a capacidade\n ");
			# Insere a capacidade, ja formatado para consulta sql
			msg += a_cap			

			# Obtem o id do predio
			p_id = raw_input(" Digite o id do predio\n ");
			msg1 += "'" + p_id + "'"

			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()
			print("\n\n")
			print message
			print("\n\n")

			# envia mensagem
			s.send(msg1)
			# recebe se obteve sucesso
			message = s.recv()

			print("\n\n")
			print message
			print("\n\n")
			
		# Inserir visitante
		elif(op == 3):
			# Adiciona a operacao, tabela e atributos
			msg = 'insert/visitante/vis_id,nome/'
			# Obtem o cpf do visitante
			v_id = raw_input("\n\n Digite o cpf do visitante\n ");
			# Insere o cpf, ja formatado para consulta sql
			msg += "'" + v_id + "',"
			# Obtem o nome do visitante			
			nome = raw_input(" Digite o nome do visitante\n ");
			# Insere o nome, ja formatado para consulta sql
			msg += "'" + nome + "'"			

			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()
			print("\n\n")
			print message
			print("\n\n")
			
			# Verifica se quer adicionar permissoes
			ac = raw_input(" Deseja inserir locais em que o visitante pode acessar? S/N\n ");
			if(ac == "S"):
				permitirAcesso(v_id)
			
		# Adicionar funcionario
		elif(op == 4):
			# Adiciona a operacao, tabela e atributos
			msg = 'insert/func/func_id,nome/'
			# Obtem o cpf do funcionario
			f_id = raw_input("\n\n Digite o cpf do funcionario\n ");
			# Insere o cpf, ja formatado para consulta sql
			msg += "'" + f_id + "',"			
			# Obtem o nome do funcionario
			nome = raw_input(" Digite o nome do funcionario\n ");
			# Insere o nome, ja formatado para consulta sql
			msg += "'" + nome + "'"			

			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()

			print("\n\n")
			print message

		# Voltar
		elif(op == 5):
			break;
##################################################################################

##################################################################################
# FUNCAO ATUALIZAR																														   #
##################################################################################

def atualizar():
	while True:
		print("\n\n##############################################################")
		print("#                           ATUALIZAR                        #")
		print("##############################################################")

		# Insere a operecao a mensagem
		msg = "update/"

		# Escolhe o lugar
		tipo = input("\n\n Escolha o lugar:\n 1 - Complexo \n 2 - Predio \n 3 - Andar\n 4 - Voltar \n ");
	
		# Se for complexo
		if(tipo == 1):
			# Insere a tabela e os atibutos na mensagem, e o complexo			
			msg += "complexo/id_complexo,capacidade/'c1',"
			# Digita a nova capacidade
			cap = raw_input("\n Digite a capacidade\n ");
			# Insere a nova capacidade na mensagem
			msg += cap

			print("\n\n")
			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()
			print message

		# Se for predio
		elif (tipo == 2):
			# Insere a tabela e os atibutos na mensagem	
			msg += "predio/id_predio,capacidade/"
			# Insere o predio a ser modificado
			p_id = raw_input("\n Digite o id do predio\n ");
			# Obtem o predio a ser modificado, ja formatado para consulta sql
			msg += "'" + p_id + "',"
			# Digita a nova capacidade
			cap = raw_input(" Digite a capacidade\n ");
			# Insere a nova capacidade na mensagem
			msg += cap

			print("\n\n")
			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()
			print message

		# Se for predio
		elif (tipo == 3):
			# Insere a tabela e os atibutos na mensagem	
			msg += "andar/id_andar,capacidade/"
			# obtem o andar a ser modificado
			p_id = raw_input("\n Digite o id do andar\n ");
			# Insere o andar a ser modificado, ja formatado para consulta sql
			msg += "'" + p_id + "',"
			# Digita a nova capacidade
			cap = raw_input(" Digite a capacidade\n ");
			# Insere a nova capacidade na mensagem
			msg += cap
	
			print("\n\n")
			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()
			print message

		# Voltar
		elif(tipo == 4):
			break

##################################################################################

##################################################################################
# FUNCAO PERMITIR 																														   #
##################################################################################

def permitirAcesso(vis_id = ""):
	while True:
		print("\n\n##############################################################")
		print("#                       PERMITIR ACESSO                      #")
		print("##############################################################")

		# Insere a operecao a mensagem
		msg = "insert/tem_acesso/"

		# Escolhe o lugar
		tipo = input("\n\n Escolha a operacao:\n 1 - Acesso a Andar \n 2 - Acesso a Predio \n 3 - Voltar \n ");
	
		# Se revogar Andar
		if(tipo == 1):
			# Adiciona a operacao, tabela e atributos
			msg += "vis_id,id_predio,id_andar/"

			# Verifica se nao vem de inserir visitante
			if(vis_id == ""):
				# Obtem o cpf do visitante
				v_id = raw_input("\n\n Digite o cpf do visitante\n ");
			else:
				v_id = vis_id

			# Obtem o predio e andar ao qual ele tem acesso
			pred = raw_input(" Id do predio ao qual ele tem acesso\n ");	
			andar = raw_input(" Id do andar ao qual ele tem acesso\n ");

			# Adiciona a mensagem, ja formatado para sql
			msg += "'" + v_id + "',"
			msg += "'" + pred + "',"
			msg += "'" + andar + "'"

			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()

			print("\n\n")
			print message
			print("\n\n")


		elif(tipo == 2):
			# Adiciona a operacao, tabela e atributos
			msg += "vis_id,id_predio/"

			# Verifica se nao vem de inserir visitante
			if(vis_id == ""):
				# Obtem o cpf do visitante
				v_id = raw_input("\n\n Digite o cpf do visitante\n  ");
			else:
				v_id = vis_id

			# Obtem o predio ao qual ele tem acesso
			pred = raw_input(" Id do predio ao qual ele tem acesso\n ");
			# Adicona a mensagem, ja formatado para sql
			msg += "'" + v_id + "',"
			msg += "'" + pred + "'"

			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()

			print("\n\n")
			print message
			print("\n\n")
		
		# Voltar
		elif(tipo == 3):
			break
##################################################################################

##################################################################################
# FUNCAO REVOGAR  																														   #
##################################################################################

def revogarAcesso():
	while True:
		print("\n\n##############################################################")
		print("#                        REVOGAR ACESSO                      #")
		print("##############################################################")

		# Insere a operecao a mensagem
		msg = "delete/"

		# Escolhe o lugar
		tipo = input("\n\n Escolha a operacao:\n 1 - Revogar Acesso a Andar \n 2 - Revogar Acesso a Predio \n 3 - Revogar todos os acessos (predios e andares) \n 4 - Voltar \n ");
	
		# Se revogar Andar
		if(tipo == 1):
			# Insere a tabela e os atibutos na mensagem, e o complexo			
			msg += "tem_acesso/vis_id,id_andar/"
			# Obtem o cpf do visitante
			v_id = raw_input("\n\n Digite o cpf do visitante\n ");
			# Insere o cpf, ja formatado para consulta sql
			msg += "'" + v_id + "',"

			# Obtem o andar ao qual ele tem acesso
			andar = raw_input(" Id do andar ao qual ele tem acesso\n ");

			# Adicona a mensagem, ja formatado para sql
			msg += "'" + andar + "'"

			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()

			print("\n\n")
			print message
			print("\n\n")

		# Se for predio
		elif (tipo == 2):
			# Insere a tabela e os atibutos na mensagem, e o complexo			
			msg += "tem_acesso/vis_id,id_predio/"
			# Obtem o cpf do visitante
			v_id = raw_input("\n\n Digite o cpf do visitante\n ");
			# Insere o cpf, ja formatado para consulta sql
			msg += "'" + v_id + "',"

			# Obtem o predio e andar ao qual ele tem acesso
			pred = raw_input(" Id do predio ao qual ele tem acesso\n ");

			# Adicona a mensagem, ja formatado para sql
			msg += "'" + pred + "'"

			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()

			print("\n\n")
			print message
			print("\n\n")

		# Se for todos os acessos
		elif (tipo == 3):
			# Insere a tabela e os atibutos na mensagem, e o complexo			
			msg += "tem_acesso/vis_id/"
			# Obtem o cpf do visitante
			v_id = raw_input("\n\n Digite o cpf do visitante\n ");
			# Insere o cpf, ja formatado para consulta sql
			msg += "'" + v_id + "'"

			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()

			print("\n\n")
			print message
			print("\n\n")

		# Voltar
		elif (tipo == 4):
			break;	
##################################################################################

##################################################################################
# FUNCAO REMOVER  																														   #
##################################################################################

def remover():
	while True:
		print("\n\n##############################################################")
		print("#                            REMOVER                         #")
		print("##############################################################")

		# Insere a operecao a mensagem
		msg = "delete/"

		# Escolhe o lugar
		tipo = input("\n\n Escolha a operacao:\n 1 - Funcionario \n 2 - Visitante \n 3 - Voltar \n ");
	
		# Funcionario
		if(tipo == 1):
			# Insere a tabela e os atibutos na mensagem, e o complexo			
			msg += "func/func_id/"
			# Obtem o cpf do visitante
			f_id = raw_input("\n\n Digite o cpf do funcionario\n ");
			# Insere o cpf, ja formatado para consulta sql
			msg += "'" + f_id + "'"

			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()

			print("\n\n")
			print message
			print("\n\n")

		# Visitante
		elif (tipo == 2):
			# Insere a tabela e os atibutos na mensagem, e o complexo			
			msg += "visitante/vis_id/"
			# Obtem o cpf do visitante
			v_id = raw_input("\n\n Digite o cpf do visitante\n ");
			# Insere o cpf, ja formatado para consulta sql
			msg += "'" + v_id + "'"

			# envia mensagem
			s.send(msg)
			# recebe se obteve sucesso
			message = s.recv()

			print("\n\n")
			print message
			print("\n\n")

		# Voltar
		elif (tipo == 3):
			break;	
##################################################################################

##################################################################################
# EXECUCAO DO ADMIN																															 #
##################################################################################
try:
	while True:
		print("\n\n##############################################################")
		print("#                             MENU                           #")
		print("##############################################################")

		# Opcoes de operacoes do admin
		op = input("\n\nEscolha a operacao:\n 1 - Mudar atributos complexo/predio/andar\n 2 - Inserir \n 3 - Permitir Acesso \n 4 - Revogar Acesso \n 5 - Deletar \n 6 - Sair\n ");

		# Modificacao dos atributos de complexo, predio ou andar
		if(op == 1):
			atualizar()
		# Inserir
		elif(op == 2):
			inserir()	
		# Permitir Acesso
		elif(op == 3):
			permitirAcesso()
		# Revogar Acesso
		elif(op == 4):
			revogarAcesso()
		# Deletar
		elif(op == 5):
			remover()
		# Sair
		elif(op == 6):
			break;

		print("\n\n")

except KeyboardInterrupt:
	print("\nFinalizando...")
finally:
	print("Finalizado!")
##################################################################################
