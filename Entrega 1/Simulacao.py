
import zmq, time
import mysql.connector
from mysql.connector import Error
from random import randint

##################################################################################
# PORTAS, HOST E VARIAVEIS																											 #
##################################################################################


HOST1 = "localhost"
HOST2 = "3.217.201.154"
HOST3 = "3.218.116.129"
HOST4 = "3.219.43.19"
PORT1 = "8083"
PORT2 = "8084"
PORT3 = "8090"
PORT4 = "8091"

# Dicionario onde mantem o lugar em que cada pessoa esta
# nao tendo como alguem entrar em um predio sem entrar no complexo
# Sendo assim, o indice/chave e o nome da pessoa
# O valor indexado e uma pilha com o local atual
# ao sair do local atual a pessoa volta ao local anterior
estado = {}
##################################################################################

##################################################################################
# CRIACAO DE SOCKETS																														 #
##################################################################################

# Socket para requisitar acesso
context1 = zmq.Context() 
p1 = "tcp://"+ HOST1 +":"+ PORT1
s1 = context1.socket(zmq.REQ)
s1.connect(p1)

# Socket para requisitar acesso
context2 = zmq.Context() 
p2 = "tcp://"+ HOST2 +":"+ PORT2
s2 = context2.socket(zmq.REQ)
s2.connect(p2)

# Socket para requisitar acesso
context3 = zmq.Context() 
p3 = "tcp://"+ HOST3 +":"+ PORT3
s3 = context3.socket(zmq.REQ)
s3.connect(p3)

# Socket para requisitar acesso
context4 = zmq.Context() 
p4 = "tcp://"+ HOST4 +":"+ PORT4
s4 = context4.socket(zmq.REQ)
s4.connect(p4)
##################################################################################

##################################################################################
# CONEXAO COM O BD E CRIACAO DE UM DICIONARIO COM PREDIOS E SEU ANDARES					 #
##################################################################################

# Conectar ao banco
try:
    connection = mysql.connector.connect(host='localhost', database='empresa', user='root', password='175932486')
    if connection.is_connected():
       db_Info = connection.get_server_info()
       print("Connected to MySQL database... MySQL Server version on ",db_Info)
       cursor = connection.cursor()
       cursor.execute("select database();")
       record = cursor.fetchone()
       print ("Your connected to - ", record)
except Error as e :
    print ("Error while connecting to MySQL", e)

# Dicionario com os predios, do tipo {predio1:[a1, a2...an], predio2:[ai, ai+1,..., am], ... prediok:[aj, aj+1, ..., az]}
predio = {}

# Obtem os predios
try:
	print("Iniciando dicionario com predios como indices e andares como conteudo...")
	predio_query = "SELECT id_predio FROM predio" 
	cursor = connection.cursor()
	cursor.execute(predio_query)
	rows1 = cursor.fetchall()

	# Cada predio vira uma chave
	for p in rows1:
		# Inicializa a lista
		predio[str(p[0])] = []

		# Pesquisa todos os andares do predio
		query = "SELECT andar.id_andar FROM esta_no, andar WHERE esta_no.id_andar = andar.id_andar AND esta_no.id_predio = '" 
		query += p[0] + "';"
		cursor = connection.cursor()
		cursor.execute(query)
		rows2 = cursor.fetchall()
	
		# Adiciona cada andar a lista
		for a in rows2:
			predio[str(p[0])].append(str(a[0]))

	print("Iniciado.")
except mysql.connector.Error as error :
	print(error)
	connection.rollback() #rollback if any exception occured
	print("Falha na operacao!")						
##################################################################################

##################################################################################
# EXECUCAO DA SIMULACAO																													 #
##################################################################################

# Parametros da simulacao
c_in = input("Insira a chance de ser uma operacao de entrada: ");
c_func = input("Insira a chance de ser um funcionario a solicitar uma operacao: ");
c_temp = input("Insira o tempo entre cada solicitacao: ");

try: 
	print("Simulacao: ")
	while True:
		# Resetar para que as atualizacoes fiquem viziveis
		connection.cmd_reset_connection()		

		# Chance de 85% de um operacao se in
		if(randint(0,100) < c_in):
			op = "IN "
		else:
			op = "OUT"

		# Chance de 60% de ser um funcionario
		if (randint(0, 100) < c_func):
			func = " func"
			select_query = "SELECT func_id FROM func;"
		else:
			select_query = "SELECT vis_id FROM visitante;"
			func = " vis"

		# Busca no BD as pessoas que tem acesso 
		cursor = connection.cursor()
		cursor.execute(select_query)
		rows = cursor.fetchall()

		# Escolhe uma pessoa aleatoria
		i = randint(0, len(rows)-1)
		pessoa = str(rows[i][0])

		# Caso a pessoa ja esteja no complexo define topo
		# (Apenas para simplificar a notacao)
		if pessoa in estado.keys():
			topo = estado[pessoa][0]

		# Se nao entrou so pode entrar no complexo
		if (pessoa not in estado.keys()):
			msg = "IN " + pessoa
			local = "c1"
			op = "IN "
		# Se esta em um andar so pode sair, nao ha para onde entrar
		elif ('a' in topo):
			msg = "OUT " + topo  + " " + pessoa
			op = "OUT "
		# Se esta em um predio e quer entrar deve procurar andares que pertecam
		# ao predio
		elif ('p' in topo and op == "IN "):
			# Escolhe um andar aleatoria que pertenca ao predio
			j = randint(0, len(predio[topo])-1)
			local = predio[topo][j]
			msg = "IN " + predio[topo][j] + " " + pessoa

		# Se esta em no complexo e quer entrar, so pode entrar em predios
		elif ('c' in topo and op == "IN "):
			# Escolhe um predio aleatorio
			j = randint(0, len(predio.keys())-1)
			pred = predio.keys()[j]
			local = pred
			msg = "IN " + pred  + " " + pessoa

		else:
			op = "OUT "
			if ('c' in topo):
				# Se nao for nenhuma das opcoes a pessoa quer sair
				msg = "OUT " + pessoa
			else:
				msg = "OUT " + topo  + " " + pessoa

		# Verfica se e o servidor central que ira atender
		if (pessoa not in estado.keys()) or ('c' in topo and op == 'OUT '):
			# Publica a mensagem, que tem a seguinte forma
			# "topico op pessoa"
			# topico e o local onde ele quer entrar ou sair
			print(msg + func)
			s1.send(msg)
			m = s1.recv()
		else:
			# Verifica qual servidor vai atender
			if ('c' in topo and op == "IN "):
				p = pred
			elif ('p' in topo):
				p = topo
			elif ('a' in topo):
				p = estado[pessoa][1]
	
			if('1' in p):
				print(msg)
				s2.send(msg)
				m = s2.recv()
			elif ('2' in p):
				print(msg)
				s3.send(msg)
				m = s3.recv()
			elif ('3' in p):
				print(msg)
				s4.send(msg)
				m = s4.recv()
			#Inserir Novo Aqui

		print(op + " " + m + func)

		# Verifica se obteve sucesso
		if(m == "Liberado"):
			# Testa se foi out					
			if (op == "OUT "):
				# Se foi out a pessoa sai do local
				# entao retiramos um local da pilha
				estado[pessoa].pop(0)
		
				# Se estiver vazio exclui a chave do dicionario
				# pessoa saiu do complexo					
				if not estado[pessoa]:
					estado.pop(pessoa)
			else:
			# Se foi in					

				# Se ele ainda nao entrou no complexo nao existe chave
				# temos que criar
				if (pessoa not in estado.keys()):
					estado[pessoa] = []
			
				# Insere o local na pilha
				estado[pessoa].insert(0, local)

		# Para melhor observacao
		time.sleep(c_temp)

except KeyboardInterrupt:
	print("\nFinalizando...")
finally:
	time.sleep(1)
	print("Finalizado!")
