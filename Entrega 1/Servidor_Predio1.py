import zmq, time
from threading import Thread

##################################################################################
# PORTAS, HOST E VARIAVEIS																											 #
##################################################################################

HOST = "*"
HOST1 = "3.212.16.17"
PORT1 = "8081"
PORT2 = "8084"
PORT3 = "8085"
PORT4 = "8086"
PORT5 = "8087"

# Dicionario com as pessoas que podem acessar o predio com os indices sendo o andar/ou predio
p_permitidas = {'p1': [], 'a1': [], 'a2': [], 'a3': [], 'a4': [], 'a5': []}

# Como funcionarios tem tratamento diferente armazena os funcionarios em uma lista diferente
func = []

# Dicionario com pessoas que entraram
p_entraram = {'p1': [], 'a1': [], 'a2': [], 'a3': [], 'a4': [], 'a5': []}

# Dicionario com a capacidade limite
lim_cap = {}

# Dicionario com o numeros de pessoas
cont = {}

##################################################################################

##################################################################################
# THREAD QUE RECEBE DO SERVIDOR CENTRAL QUEM PODE TER ACESSO										 #
##################################################################################

class Atualiza(Thread):
# Thread que atualiza ao receber uma publicao
	def __init__ (self):
		Thread.__init__(self)

	def run(self):
		# Define uma porta para escutar as publicacoes
		# Cria o socket
		context1 = zmq.Context()
		s_atualiza = context1.socket(zmq.SUB)
		p_atualiza = "tcp://"+ HOST1 +":"+ PORT1
		s_atualiza.connect(p_atualiza)
		s_atualiza.setsockopt(zmq.SUBSCRIBE, "p1")
		s_atualiza.setsockopt(zmq.SUBSCRIBE, "func")

		# Espera uma publicacao
		while True:
			message = s_atualiza.recv()

			print("Atualizacao acesso: " + message)

			# Obtem os campos da mensagem
			aux = message.split(" ")

			# Se for funcionario pode acessar todos os andares
			if (aux[0] == "func"):
				# Se o funcionario foi removido
				if (aux[1] == "rm"):
					# Verifica se existe
					if (aux[2] in func):
						# Remove da lista de funcionarios presentes no andar
						func.remove(aux[2])					

				# Se o funcionario entrou no complexo
				else:
					# Verifica se ja existe
					if (aux[1] not in func):
						# Adiciona a lista de funcionario presentes no andar
						func.append(aux[1])

			# Visitante nao tem acesso a todos os lugares
			else:
				# Se o acesso foi revogado ou o visitante removido
				if (aux[1] == "rm"):
					# Verifica se existe
					if (aux[2] in p_permitidas[aux[3]]):
						p_permitidas[aux[3]].remove(aux[2])
				else:
				# Se o visitante entrou
					# Verifica se ja existe
					if (aux[1] not in p_permitidas[aux[2]]):
						p_permitidas[aux[2]].append(aux[1])
##################################################################################

##################################################################################
# THREAD QUE RECEBE DO SERVIDOR CENTRAL ATUALIZACOES DAS CAPACIDADES						 #
##################################################################################

class AtualizaCap(Thread):
# Thread que atualiza ao receber uma publicao
	def __init__ (self):
		Thread.__init__(self)

	def run(self):
		# Define uma porta para escutar as publicacoes
		# Cria o socket
		context4 = zmq.Context()
		s_atualizaU = context4.socket(zmq.SUB)
		p_atualizaU = "tcp://"+ HOST1 +":"+ PORT4
		s_atualizaU.connect(p_atualizaU)
		s_atualizaU.setsockopt(zmq.SUBSCRIBE, "'p1'")
		s_atualizaU.setsockopt(zmq.SUBSCRIBE, "'a1'")
		s_atualizaU.setsockopt(zmq.SUBSCRIBE, "'a2'")
		s_atualizaU.setsockopt(zmq.SUBSCRIBE, "'a3'")
		s_atualizaU.setsockopt(zmq.SUBSCRIBE, "'a4'")
		s_atualizaU.setsockopt(zmq.SUBSCRIBE, "'a5'")

		while True:
			# Recebe atualizacao
			message = s_atualizaU.recv()

			print("Atualizacao Capacidade: " + message)

			# Obtem campos
			aux = message.split(" ")
			# Atualiza o dicionario
			lim_cap[aux[0]] = int(aux[1])
##################################################################################		

##################################################################################
#THREAD QUE ATENDE REQUISICOES DE ACESSO DO DISPOSITIVO DE ACESSO AO PREDIO E DO #
#DISPOSITIVO DE ACESSO A UM ANDAR																								 # 
##################################################################################

class Permissao(Thread):
	# Thread para atender as requisicoes do(s) dispositivo(s) de acesso
	# na entrada do predio
	def __init__ (self):
		Thread.__init__(self)

	def run(self):
		# Funciona em requisicao e resposta
		# Usa uma porta para atender as requisicoes do dispositivo de acesso
		# Cria um socket
		context2 = zmq.Context()
		p_predio = "tcp://"+ HOST +":"+ PORT2
		s_predio = context2.socket(zmq.REP)
		s_predio.bind(p_predio)
		
		# Espera uma requisicao do dispositivo de acesso
		while True:
			# Recebe a requisicao do dispositivo de acesso
			message = s_predio.recv()

			# Obtem os campos
			aux = message.split(" ")

			# Cria um indice formantando da forma correta, apenas para facilitar acesso
			i = "'" + aux[1] + "'"

			# Verifica se alguem quer entrar
			if (aux[0] == "IN"):
				#Testa Permissao
				if ((aux[2] in func or (aux[2] in p_permitidas[aux[1]] and cont[i] < lim_cap[i])) and aux[2] not in p_entraram[aux[1]]):
					# Incrementa contador
					cont[i] += 1
					# Adiciona nas pessoas que entraram
					p_entraram[aux[1]].append(aux[2])
					# Envia mensagem
					s_predio.send("Liberado")
				else:
					# Envia mensagem					
					s_predio.send("Bloqueado")

			# Verifica se alguem quer sair
			elif (aux[0] == "OUT"):
				# Verfica se a pessoa esta no predio/andar
				if(aux[2] in p_entraram[aux[1]]):
					# Diminui contador
					cont[i] -= 1
					# Retira da lista de pessoas presentes
					p_entraram[aux[1]].remove(aux[2])
					# Envia resposta
					s_predio.send("Liberado")						
				else:
					# Envia resposta
					s_predio.send("Bloqueado")	

			print("Requisicao Acesso: " + message + " Cont: " + str(cont[i]) + " Max: " + str(lim_cap[i]))					
##################################################################################

##################################################################################
# REQUISITA AO SERVIDOR CENTRAL O LIMITE DA CAPACIDADE DO PREDIO E SEUS ANDARES	 #
##################################################################################

# Para finalizar sem erros ao aperta ctlr-c
try:
	print("Obtendo Limite da Capacidade...")

	# Cria o socket
	context5 = zmq.Context()
	p_ini = "tcp://"+ HOST1 +":"+ PORT5
	s_ini = context5.socket(zmq.REQ)
	s_ini.connect(p_ini)

	# Requisita os limites para o predio
	s_ini.send("'p1'")
	m = s_ini.recv()

	# Adiciona no dicionario e inicializa contador
	aux = m.split(" ")
	for a in aux:
		aux2 = a.split(":")
		lim_cap["'" + str(aux2[0]) + "'"] = int(aux2[1])
		cont["'" + str(aux2[0]) + "'"] = 0

	print("Limites obtidos.")
##################################################################################

##################################################################################
# CHAMADA DAS THREADS													 																	 #
##################################################################################
	print("SERVIDOR PREDIO: P1")
	p1 = Permissao()
	p1.daemon = True;
	atualiza = Atualiza()
	atualiza.daemon = True;
	atualizaCap = AtualizaCap()
	atualizaCap.daemon = True;
	p1.start()
	atualiza.start()
	atualizaCap.start()

	while True:
	 time.sleep(1)
except KeyboardInterrupt:
	print("\nFinalizando...")
finally:
	s_ini.close()
	time.sleep(1)
	print("Finalizado!")
##################################################################################
