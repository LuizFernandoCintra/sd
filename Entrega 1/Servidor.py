import zmq, time
import mysql.connector
from mysql.connector import Error
from threading import Thread

##################################################################################
# PORTAS, HOST E VARIAVEIS																											 #
##################################################################################
HOST = "*"
PORT1 = "8081"
PORT2 = "8082"
PORT3 = "8083"
PORT4 = "8086"
PORT5 = "8087"

# Pessoa que entraram
p_entraram = []

# Contador de pessoas
cont = 0

##################################################################################

##################################################################################
# CRIACAO DE SOCKETS PUB/SUB																										 #
##################################################################################

# Usa uma porta para publicar atualizacoes de acesso
context1 = zmq.Context()
s_publica = context1.socket(zmq.PUB)
p_publica = "tcp://"+ HOST +":"+ PORT1
s_publica.bind(p_publica)

# Usa uma porta para publicar atualizacoes das capacidades
context4 = zmq.Context()
s_publicaCap = context4.socket(zmq.PUB)
p_publicaCap = "tcp://"+ HOST +":"+ PORT4
s_publicaCap.bind(p_publicaCap)

##################################################################################

##################################################################################
# COMUNICACAO COM O BD																													 #
##################################################################################

# Conectar ao banco
try:
    connection = mysql.connector.connect(host='localhost', database='empresa', user='root', password='175932486')
    if connection.is_connected():
       db_Info = connection.get_server_info()
       print("Connected to MySQL database... MySQL Server version on ",db_Info)
       cursor = connection.cursor()
       cursor.execute("select database();")
       record = cursor.fetchone()
       print ("Your connected to - ", record)
except Error as e :
    print ("Error while connecting to MySQL", e)

# Consulta a capacidade do complexo
select_query = "SELECT capacidade FROM complexo;"
try:
	print("\n\nInicializando a capacidade limite...")
	cursor = connection.cursor()
	cursor.execute(select_query)
	rows = cursor.fetchall()

	# Inicializa a capacidade do complexo
	lim = int(rows[0][0])

	print("Inicializado.")
except mysql.connector.Error as error :
	print(error)
	connection.rollback() #rollback if any exception occured
	print("Falha na operacao!")
##################################################################################

##################################################################################
# THREAD QUE INICIALIZA A CAPACIDADE DOS SERVIDORES LOCAIS											 #
##################################################################################

class Inicializar(Thread):
	# Thread que espera uma requisicao de um servidor local 
	# para inicializar suas capacidades limite
	def __init__ (self):
		Thread.__init__(self)

	def run(self):
		# Funciona em requisicao e resposta
		# Criacao do socket
		context5 = zmq.Context()
		p_ini = "tcp://"+ HOST +":"+ PORT5
		s_ini = context5.socket(zmq.REP)
		s_ini.bind(p_ini)

		while True:
			# Recebe a requisicao (o id do predio)
			message = s_ini.recv()
			print("Pedido para obter capacidades: " + message)
		
			# Consulta a capacidade do predio e seus andares
			select_query = "SELECT * FROM predio WHERE id_predio = " + message + ";"
			try:
				cursor = connection.cursor()
				cursor.execute(select_query)
				rows = cursor.fetchall()

				# Mensagem do tipo "local:capacidade local:capacidade ... local:capacidade"
				msgm = str(rows[0][0]) + ":" + str(rows[0][1])

				# Consulta capacidade andar
				select_query_andar = "SELECT andar.capacidade, andar.id_andar FROM esta_no, andar"
				select_query_andar += " WHERE esta_no.id_andar = andar.id_andar AND esta_no.id_predio = " + message + ";"

				cursor = connection.cursor()
				cursor.execute(select_query_andar)
				rows1 = cursor.fetchall()

				# Adiciona cada andar e sua capacidade
				for a in rows1:
					msgm += " " + str(a[1]) + ":" + str(a[0])

				print("Capacidades inicializadas: " + msgm)
				# Envia mensagem
				s_ini.send(msgm)			

			except mysql.connector.Error as error :
				print(error)
				connection.rollback() #rollback if any exception occured
				print("Falha na operacao!")						
##################################################################################

##################################################################################
# THREAD QUE ATENDE REQUISICOES DO ADMIN																				 #
##################################################################################

class Admin(Thread):
	#Thread onde o Admin modifica ou insere
	def __init__ (self):
		Thread.__init__(self)

	def run(self):
		global lim

		# Funciona em requisicao e resposta
		# Usa uma porta para atender as requisicoes do Admin
		# Cria socket
		context2 = zmq.Context()
		p_admin = "tcp://"+ HOST +":"+ PORT2
		s_admin = context1.socket(zmq.REP)
		s_admin.bind(p_admin)
		
		# Espera uma requisicao do admin
		while True:
			# Recebe a requisicao do admin
			message = s_admin.recv()

			print("Atualizacao Admin: " + message)

			# Obtem cada campo da mensagem
			aux = message.split('/')
			op = aux[0]
			tabela = aux[1]
			campos = aux[2]
			valores = aux[3] 

			# Cria query
			if(op == 'insert'):
				sql_insert_query = "INSERT INTO " + tabela + "(" + campos + ") VALUES (" + valores + ");"

			elif (op == 'update'):
				aux2 = valores.split(',')
				aux3 = campos.split(',')
				sql_insert_query = "UPDATE " + tabela + " SET " + aux3[1] + " = " + aux2[1] + " WHERE " + aux3[0] + " = " + aux2[0] + ";"

			elif (op == 'delete'):
				aux2 = valores.split(',')
				aux3 = campos.split(',')

				# Query para deletar
				sql_insert_query = "DELETE FROM " + tabela + " WHERE "  + aux3[0] + " = " + aux2[0]

				# Constroi query para obter os lugares onde o visitante tem acesso
				# e consulta o BD, para que se possa revogar caso ja estejam no complexo
				select_query3 = "SELECT * FROM tem_acesso WHERE " + aux3[0] + " = " + aux2[0]

				# Insere as clausulas pos WHERE
				for k in range(1, len(aux3)):				
					sql_insert_query += " AND " + aux3[k] + " = " + aux2[k]
					select_query3 += " AND " + aux3[k] + " = " + aux2[k]

				sql_insert_query += ";"
				select_query3 += ";"

				# Para cada tupla <topico, visitante, lugar> cria uma mensagem("topico rm visitante lugar")
				msg_revoga = []

				# Deve guardar apenas para o visitante, porque o funcionario tem acesso a tudo
				# logo se deleta um funcionario todos os servidores locais sao notificados nao precisa
				# notificar apenas os que tem acesso como no visitante
				if(tabela == "visitante" or tabela == "tem_acesso"):
					cursor = connection.cursor()
					cursor.execute(select_query3)
					rows1 = cursor.fetchall()

					for acesso in rows1:
						# Testa se acesso somente predio
						if(acesso[1] == None):
							msg_revoga.append(str(acesso[2]) + " rm " + str(acesso[0]) + " " + str(acesso[2]))
						else: 
						# Acesso predio andar
							msg_revoga.append(str(acesso[2]) + " rm " + str(acesso[0]) + " " + str(acesso[1]))


				# Se for visitante deve excluir as suas permissoes de acesso
				if(tabela == "visitante"):
					# Query para deletar
					delete_query = "DELETE FROM tem_acesso WHERE vis_id = " + aux2[0]					

					try:
						cursor = connection.cursor()
						cursor.execute(delete_query)
						connection.commit()
					except mysql.connector.Error as error :
						connection.rollback() #rollback if any exception occured
						s_admin.send("Falha na operacao!\n" + str(error))

			#Insere, modifica ou remove
			# OBS: com requisicao/resposta tem que haver resposta senao da erro
			# pois bloqueia o recv ate que haja um send.
			try:
				cursor = connection.cursor()
				result  = cursor.execute(sql_insert_query)
				connection.commit()
				# Envia confirmacao da operacao
				s_admin.send("Operacao realizada com sucesso!")

				# Se for alterou algum parametro dos predios/andar deve-se publicar alteracoes
				if(op == "update" and tabela != "complexo"):
					msg = aux2[0] + " " + aux2[1]
					print("Altera Capacidade: " + msg)
					s_publicaCap.send(msg)

				# Se alterou parametro do complexo atualiza o limite
				elif(op == "update" and tabela == "complexo"):
					lim = int(aux2[1])
					print("Limite alterado: " + str(lim))

				# Deletou ou revogou acesso
				elif(op == "delete"):
					# Formata para acesso posterior. Pois nao tem aspas simples
					nome = aux2[0].replace("'", "")

					# Se o funcionario for excluido e estiver no complexo
					# revoga seu acesso
					if(tabela == "func" and nome in p_entraram):
						# Mensagem do tipo <topico, rm, visitante, lugar>
						msg = tabela + " rm " + nome
						print("Revoga: " + msg)
						s_publica.send(msg)

					# Se o visitante for excluido ou tiver seu acesso revogado e ja
					# estiver dentro do complexo revoga seu acesso
					elif (nome in p_entraram):
						# Lista com mensagens para revogar o acesso
						for p in msg_revoga:
							print("Revoga: " + p)
							s_publica.send(p)

				# Se estamos permitindo acesso e o visitante ja entrou deve
				# atualizar
				elif(op == "insert" and tabela == "tem_acesso"):
					# Formata para ser o topico. Pois este e sem aspas simples
					valores = valores.replace("'", "")
					div = valores.split(',')

					# Se permissao para um andar, entao ha tres valores, pois nao e passado 
					# valor para o andar 
					if(len(div) == 3 and div[0] in p_entraram):
						# Mensagem do tipo <topico pessoa local>
						msg = str(div[1]) + " " + div[0] + " " + str(div[2])
						print("Atualizacao: " + msg)
						s_publica.send(msg)

					# Se permissao para predio
					elif(div[0] in p_entraram):
						# Mensagem do tipo <topico pessoa local>
						msg = str(div[1]) + " " + div[0] + " " + str(div[1])
						print("Atualizacao: " + msg)
						s_publica.send(msg)

				# Podemos ter inserido um funcionario que foi removido durante a execucao e estava 
				# em algum lugar no complexo. Caso isso ocorra se nao atualizarmos o servidor local 
				# ele nao podera entrar nos locais, mesmo depois de ser inserido novamente
				elif(op == "insert" and tabela == "func"):
					# Formata para ser o topico. Pois este e sem aspas simples
					valores = valores.replace("'", "")
					div = valores.split(',')

					# Se entrou no complexo
					if(div[0] in p_entraram):
						# Mensagem do tipo <topico pessoa local>
						msg = tabela + " " + div[0]
						print("Atualizacao: " + msg)
						s_publica.send(msg)

			except mysql.connector.Error as error :
				connection.rollback() #rollback if any exception occured
				s_admin.send("Falha na operacao!\n" + str(error))
##################################################################################

##################################################################################
# THREAD QUE ATENDE REQUISICOES DE ENTRADA DO DISPOSITIVO DE ACESSO DA ENTRADA DO#	
#	COMPLEXO																		 																	 #
##################################################################################

class Permissao(Thread):
	# Thread para atender as requisicoes do(s) dispositivo(s) de acesso
	# na entrada do complexo
	def __init__ (self):
		Thread.__init__(self)

	def run(self):
		global cont
		global lim
		global p_entraram

		# Funciona em requisicao e resposta
		# Usa uma porta para atender as requisicoes do dispositivo de acesso
		# Cria socket
		context3 = zmq.Context()
		p_entrada = "tcp://"+ HOST +":"+ PORT3
		s_entrada = context3.socket(zmq.REP)
		s_entrada.bind(p_entrada)

		# Espera uma requisicao do dispositivo de acesso
		while True:
			# Recebe a requisicao do dispositivo de acesso
			m = s_entrada.recv()

			# Obtem os campos
			message = m.split(" ")

			# Verifica se alguem quer entrar
			if (message[0] == "IN"):
				# Constroi as querys
				select_query1 = "SELECT * FROM func WHERE func_id = '" + message[1] + "';"
				select_query2 = "SELECT * FROM visitante WHERE vis_id = '" + message[1] + "';"

				# Se ja entrou descarta
				if (message[1] not in p_entraram):

					try:
						#Testa Permissao funcionario vendo se ele esta no BD
						cursor = connection.cursor()
						cursor.execute(select_query1)
						rows = cursor.fetchall()

						# Verfica se o funcionario tem acesso
						if (rows):
							# Atualiza o contador
							cont += 1
							# Coloca na lista de pessoas que entraram
							p_entraram.append(message[1])

							# Envia mensagem liberando a entrada
							s_entrada.send("Liberado")

							# Publica a mensagem para todos os servidores locais
							# como funcionario ele pode acessar todos os lugares
							s_publica.send("func " +  message[1])

						#Testa Permissao visitante
						#Se o limite for atingido
						elif(cont < lim):
							cursor = connection.cursor()
							cursor.execute(select_query2)
							rows = cursor.fetchall()

							# Verfica se o visitante tem acesso consultando o BD
							if (rows):
								# Constroi query para obter os lugares onde o visitante tem acesso
								# e consulta o BD
								select_query3 = "SELECT * FROM tem_acesso WHERE vis_id = '" + message[1] + "';"
								cursor = connection.cursor()
								cursor.execute(select_query3)
								rows1 = cursor.fetchall()

								# Para cada tupla <topico, visitante, lugar> envie s_publica.send("topico visitante lugar")
								for acesso in rows1:
									if(acesso[1] == None):
										msg = str(acesso[2]) + " " + message[1] + " " + str(acesso[2])
									else: 
										msg = str(acesso[2]) + " " + message[1] + " " + str(acesso[1])

									s_publica.send(msg)

								# Atualiza o contador								
								cont += 1
								# Coloca na lista de pessoas que entraram
								p_entraram.append(message[1])

								# Envia mensagem liberando a entrada
								s_entrada.send("Liberado")

							# Se nao e visitante, nem funcionario
							else:
								# Envia mensagem bloqueando a entrada
								s_entrada.send("Bloqueado")
						# Se excedeu o limite e nao e funcionario
						else:
							# Envia mensagem bloqueando a entrada
							s_entrada.send("Bloqueado")

					except mysql.connector.Error as error :
						print(error)
						connection.rollback() #rollback if any exception occured
						s_entrada.send("Falha na operacao!")						

				else:
					# Envia mensagem bloqueando a entrada
					s_entrada.send("Bloqueado")

			# Verifica se alguem quer sair
			elif (message[0] == "OUT"):
				# Verfica se a pessoa esta no complexo
				if(message[1] in p_entraram):
					# Diminui contador
					cont -= 1
					# Retira da lista de pessoas presentes
					p_entraram.remove(message[1])
					# Envia resposta
					s_entrada.send("Liberado")						
				else:
					# Envia resposta
					s_entrada.send("Bloqueado")

			print("Requisicao: " + m + " Cont: " + str(cont) + " Max: " + str(lim))
##################################################################################

##################################################################################
# CHAMADA DAS THREADS													 																	 #
##################################################################################
try:
	admin = Admin()
	admin.daemon = True
	permissao = Permissao()
	permissao.daemon = True
	ini = Inicializar()
	ini.daemon = True
	admin.start()
	permissao.start()
	ini.start()

	while True:
	 time.sleep(1)
except KeyboardInterrupt:
	print("\nFinalizando...")
finally:
	time.sleep(1)
	print("Finalizado!")
##################################################################################
